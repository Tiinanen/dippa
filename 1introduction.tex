\chapter{Introduction}
\label{chapter:intro}

\section{Background and motivation}

Organizations and social groups can be naturally perceived as systems, i.e. wholes consisting
of multiple mutually interacting parts, where the interactions often include non-linearities and
feedback loops. Such systems are seldom observed
from the outside, but rather we are surrounded within them. Acting constructively within a social
system and positively affecting its state is a complex problem, since the effects 
of the system, such as non-linearities, feedbacks and time delays, are difficult
or even impossible to understand. Consequently, tools that would facilitate perceiving the
systemic nature of the problem could be beneficial to our understanding of the everyday systems,
ultimately leading to a more productive behaviour within them.

Although the dynamics of social systems can be difficult to understand, 
humans do have a remarkable capability to act intelligently within different systems,
a concept known as systems intelligence \citep{saarinen2007}.
A systems intelligent person perceives the system as a whole and
recognizes herself as an active part of the system, who is both able to affect the state of the
system and is reciprocally influenced herself by the system. She can act productively inside
the system and is able to recognize and take advantage of different feedback mechanisms.
Some individuals are more proficient than others in acting intelligently within systems
such as different social groups, but it is a skill that can be developed. To study
systems intelligence within social groups, the Systems Intelligence
research group has previously developed a simulator called \emph{Positive Systems Intelligent Teams} (PoSITeams) \citep{tormanen}.
PoSITeams is a web-based multi-agent social simulator
that simulates the dynamics and evolution of positive and negative affect in a team.
Agent-based simulations have been used extensively to model social systems
and they can provide useful insights into the underlying systems 
and introduce ideas to improve their performance.

We are social animals and we are greatly influenced by the emotions of others.
Emotions have been widely studied in psychology \citep{frijda1986} and a lot is known about
their effects on individuals and social groups. 
Positive emotional contagion has been linked to increased performance in social groups \citep{barsade2002}. 
In particular, the ratio of positive and negative affect has proven to be an especially
useful parameter. It has been successfully applied to predicting effective organizations and 
successful marriages \citep{losada2004, gottman1994}. Positive emotions have been studied in the field of
\emph{positive psychology}, which focuses on human flourishing and how to improve our lives contrary
to the traditional fields of psychology, which concentrate on the remedies to psychological problems
\citep{seligman2000}. Also on the individual level the characteristic difference between flourishing
and non-flourishing individuals has been observed to be the
ratio of experienced positive and negative emotions \citep{fredrickson2013}.
Positivity ratios can therefore be used as indicators of the overall performance and well-being of
both social systems as well as its individuals, which has been the motivation behind PoSITeams.

The purpose of PoSITeams is to enable the user to simulate social groups of her own and 
explore the effects of different behavioural changes. The focus is especially in 
engaging the user in reflective thought-processes and facilitate seeing the system as a whole and 
let the user recognize herself as an active part of the system. In this sense, the simulator
can be used to promote systems intelligent behaviour in a social context.

\section{Research objectives}

PoSITeams has several interesting directions for further development. 
The objective of this work is to extend the mathematical model behind the PoSITeams simulator to
make it capable of representing a wider range of behavioural patterns. The model should be
based on psychological research and it should be able to describe real psychological phenomena.
Existing models in the literature are also taken into account.
A new web-based simulator is implemented, which simulates the proposed model in actual
organizations and social groups. Similarly to PoSITeams, the purpose of the simulator
is to let the user to explore the effects of different behavioural and structural changes
within her organization in a way that promotes a systems intelligent perspective of the organization.
However, the extended mathematical model enables the user include a wider range of dependencies
while specifying social groups. These parameters can be easily changed to try
different behavioural and structural changes within the system. 
The proposed emotional contagion model is also considered as an optimization problem, which enables the simulator
to suggest systems intelligent actions by optimizing the total positivity of the group.

\section{Structure of the thesis}

The thesis is structured as follows: chapter 2 covers the theoretical background behind the
work, including systems intelligence and the effects of positive and negative affect and their
relation to the performance of organizations. Existing mathematical models for emotional contagion
are also discussed.
Chapter 3 presents a new mathematical model for emotional contagion and its mathematical formulation,
followed by the optimization methods used in the simulator. Chapter 4 discusses the implementation
and features of the simulator. Chapter 5 demonstrates the properties of the model by providing
example simulations. Chapter 6 considers potential applications for the simulator and studies
the feasibility of applying the emotional contagion models to email data using the Enron Corpus.
Chapter 7 reflects various aspects of the work and suggests directions for future research.