\chapter{Theoretical background}
\label{chapter:background} 

\section{Systems intelligence}

The concept of systems intelligence was introduced by Professors Raimo P. Hämäläinen
and Esa Saarinen of Aalto University in 2004. Systems intelligence can be defined as the ability to act
intelligently within complex systems, i.e. wholes consisting of different parts with
complicated interactions, dynamics and feedback loops \citep{saarinen2007}. The conceptual basis of
systems intelligence has been greatly
influenced by systems thinking, especially by the highly acclaimed work of \citet{senge1990}.
Both of these systems approaches emphasize the holistic view of perceiving the world through
interconnectivity and interdependence of its components rather than reducing the whole to its parts.
They share the tenet that the whole
is greater than its parts and that there are emergent phenomena that are not reducible to the properties
of these parts. However, systems intelligence focuses on human behaviour within systems, rather
than attempting to understand the system from the outside, which is characteristic for systems thinking.
In systems intelligence, a person is recognized as an active
part within the system with some power to affect its state, while being reciprocally influenced
by the system. It is recognized that everyday systems have uncertainties, but they might still require taking action.
Systems intelligence therefore strives to be an intuitive concept that brings new perspectives
to everyday issues, leading to concrete actions.

Systems intelligence is conceptually related to the theory of multiple intelligences \citep{howard1983}
and emotional intelligence \citep{goleman1995}. Systems intelligence is, however, considered to be a higher level
competence, which is not directly reducible to these forms of intelligences \citep{saarinen2010}.
Since systems intelligence also looks
for opportunities for improvement within systems, it is also connected to positive psychology
\citep{seligman2000}, a field of psychology focusing on how to live better rather than finding remedies
to psychological problems. Systems intelligence is considered to be a combination of eight distinct capabilities: systems perception, attunement, reflection, positive engagement, spirited discovery, effective responsiveness, wise action and positive attitude \citep{beingbetterbetter}. These dimensions can be grouped roughly into four categories:
perceiving, attitude, thinking and acting, as shown in table \ref{table:sifactors}.

\bgroup
\def\arraystretch{2}
\begin{table}[h!]
\begin{tabular}{ r|c|c| }
\cline{2-3}
Perceiving & Systems Perception & Attunement \\
\cline{2-3}
Attitude & Positive Attitude & Spirited Discovery \\
\cline{2-3}
Thinking & Reflection & Wise Action \\
\cline{2-3}
Acting & Positive Engagement & Effective Responsiveness \\
\cline{2-3}
\end{tabular}
\caption{The eight dimensions of systems intelligence.}
\label{table:sifactors}
\end{table}
\bgroup
\def\arraystretch{1}

Organizations can be naturally considered as systems, which makes systems intelligence
a particularly useful concept to leadership and organizational life \citep{hamalainen2008, hamalainen2007}.
Most organizations have a clearly defined goal, and systems intelligent behaviour in such a context
is therefore finding actions within the organization that make it more effective at reaching its goals.
Especially in leadership positions the potential to influence the system is large, which makes
systems intelligence a key competence of a successful leader.

Organizations are examples of social groups. An important feature
of social groups is that one seldom has an opportunity to view them
from the outside, but rather one is an active part of the system with some power to affect its
state. Social groups are therefore a great environment for the analysis of systems intelligence.
For example a simple encouragement might have a surprisingly significant effect depending
on the group. Humans are social animals, so we have a
fairly developed innate ability to understand social groups and hence we are systems
intelligent to some degree. However, systemic features such as non-linear interactions, feedbacks,
accumulation and time delays can be difficult to grasp intuitively. Therefore humans can
have difficulties to see the potential that a simple act, such as an encouragement, might have
on the group. Similarly it can be difficult the see the extent to which negative behaviour is
detrimental for the group. Systems intelligence in a social context therefore requires an understanding
of emotion dynamics.

\section{Effects of positive and negative emotions}

Since an encouragement or a criticism can have a great impact on individuals, and
therefore on the whole group, it is no surprise that negative and positive affect may serve
as an indication of how well a group of people function together.

The broaden-and-build theory in positive psychology suggests that positive
emotion have a much larger role than merely to make one "feel good" or indicating emotional well-being
\citep{fredrickson1998, fredrickson2001}. There is empirical evidence that experiencing positive emotions increases
awareness and openness to considered a wider selection of thoughts and actions (see e.g. \citet{fredrickson2005, schmitz2009}). Contrary to negative
emotions, that are often associated with narrow thought-action repertoires which are quite specific to
cope with event that induces the negative reaction (e.g. fear tends to elicit
a fight-or-flight response), positive emotions such as joy promotes playfulness, curiosity and interest,
which can turn into a wide selection of different thoughts and actions. Through such positivity-induced
actions, a person then builds her cognitive, social, psychological, emotional and physical resources 
that will be long lasting, unlike the fleeting emotions evoking this process \citep{fredrickson1998, fredrickson2001}.

Since experiencing positive emotions broadens one's thought-action repertoires and builds personal
resources, positive emotions increase flexibility and ability to cope with adversities \citep{fredrickson2003, garland2010}. Therefore
people experiencing positive emotions are more resilient against negative emotions and they are more likely
to experience more positive experiences in the future, creating a positive feedback loop towards emotional
well-being \citep{fredrickson2002spiral}. Negative emotions also have a potential to turn into feedback
loops, as is often observed in depression \citep{garland2010}.

An important concept in positive psychology is \emph{flourishing}, which is "to live within an optimal range of human functioning, one that connotes goodness, generativity, growth, and resilience" \citep{losada2005}. The characteristic difference between flourishing and non-flourishing individuals appears to be the
ratio of experienced positive and negative emotions \citep{losada2005, fredrickson2013}. A commonly used estimate for this tipping point
has been 3:1, although it might not be universally applicable to all demographics \citep{fredrickson2013}.
Humans have a tendency to experience negative emotions more strongly than positive ones \citep{baumeister2001, rozin2001},
which might explain the asymmetry seen in the positivity ratio. This \emph{negativity bias} seems to be
relatively weaker for the flourishing individuals, who have a stronger reaction to positive everyday events \citep{catalino2011}.
Another asymmetry between positive and negative emotions is the \emph{positivity offset}, which is tendency
to experience most neutral situations as mildly positive \citep{cacioppo1999}.

People have a tendency to be influenced by the emotions of others, known as \emph{emotional contagion}, which
has been defined by \citet{hatfield1994} as:

\begin{quote}
"a tendency to automatically mimic and synchronize expressions, vocalizations, postures, and movements 
with those of another person's and, consequently, to converge emotionally"
\end{quote}
 
That is, people do not experience emotions and moods in isolation, but they are largely affected by the surrounding
people, often unknowingly. Groups can experience collective emotional states, which are not directly reducible to the individuals
of the group. This is referred to as the "top-down" view of group emotions, where the group is seen as an emotional entity that shapes
the emotional responses of its individuals \citep{barsade1998}. However, \citet{barsade1998} argue that
group emotions should also be viewed from a "bottom-up" perspective, where the composition of the individuals construct the emotional state of the group.

Positive emotional contagion has been linked to increased performance in social groups \citep{barsade2002}.
In particular, the positivity ratio is not merely related to individuals, but similar observations have also been made for social groups.
For example successful marriages tend to have
a ratio of positive and negative interactions around 5:1 \citep{gottman1994}. Similarly high
performance business teams seem to have a positivity ratio of 5:1 \citep{losada2004}.
High positivity ratio also increases the number of strong connections in the group,
referred to as \emph{connectivity}
\citep{losada2004}. Similar observation has also been noted in \citet{waugh2006}, where positive emotions
were shown to increase the feeling of "oneness" in the group, which could be interpreted as a form of connectivity. There is also empirical evidence that
positive emotions increase sociability \citep{whelan2012}, also a means to increase connectivity of the group.

\section{Modelling the effects of emotions}

\subsection{Representing emotions}
There is a wide range of different emotions, both positive and negative \citep{frijda1986}.
There have been several attempts to identify a discrete set of fundamental basic emotions
that are cross-culturally recognized and that can explain more complicated emotions (see e.g
\citet{ekman1971, plutchik2001, jack2014}). Although there is no consensus on the number
of basic emotions \citep{ortony1990}, one approach to modelling emotions could be to select a subset of them to be represented separately.
Emotional contagion of different basic emotions has been studied in \citet{doherty1997}.
However, it is more common to represent emotions with different dimensional models, which
usually have two to three different dimensions to describe the emotions \citep{marsella2010}.
Typical parameters for these models are valence, which represents emotion in the negativity-positivity continuum,
and arousal, which indicates the intensity of the subjective emotion parameters.
For example, hate is a highly aroused state with negative valence, whereas boredom would be a state
with negative valence and low level of arousal. Examples of dimensional models of emotion
are, for example, the circumplex model \citep{russell1980}, which uses valence and arousal dimensions,
and the PAD model \citep{mehrabian1980}, which also incorporates dominance-submissiveness dimension.
For instance, hate and fear are examples of dominant and submissive emotions.

The dimensional models of emotions are mostly concerned about representing different emotions.
However, the interest of this work is the effects of positivity and negativity in organizations,
so there is no need to represent different emotions and it is natural to model them only in terms of their impact on positivity and
negativity. This also greatly simplifies the model since the complex interplay of different
emotions and also their arousal/dominance aspect can be omitted. Therefore only models
that concentrate on positivity and negativity are considered in this work.

One interesting note is that it is common to represent mood
by its positivity, so the simplification of modeling emotions by classifying them into
positive and negative might capture some other affective phenomena such as mood. The main
distinctive difference between mood and emotion is that mood is generally a much longer
lasting phenomenon, whereas emotions usually only last at most a couple of hours \citep{frijda1993}.



\subsection{Emotional contagion models}
Although John M. Gottman does not use the term "emotional contagion",
his research on marital happiness is highly relevant \citep{gottman1994, gottman2002}.
Gottman models the interaction between husband and wife with equations

\begin{align}
\label{eq:gottman}
\begin{split}
W_{t+1} &= I_{HW}(H_t) + r_1 W_t + a \\
H_{t+1} &= I_{WH}(W_t) + r_2 H_t + b,
\end{split}
\end{align}

where $W_{t}$ and $H_{t}$ represent the emotional states of the wife and the husband
respectively at time $t$. Husband and wife affect each other through their
\emph{influence function} $I_{HW}$ and $I_{WH}$, which are bilinear functions of the
influencing partner's current emotional state. The rest of the equation
represents the \emph{uninfluenced} part, which describes the behaviour of the spouse
when there is no interaction between the partners. Parameters $r_1$ and $r_2$ are
called \emph{emotional inertia} that describe how quickly the emotional
states approach to their steady states. The parameters $a$ and $b$ do not
have an intuitive interpretation, but they affect the dynamics of the model.
Gottman has also extended the model \ref{eq:gottman} with additional correction terms \citep{gottman2002}.

Agent-based simulations have been used extensively to model phenomena in social sciences (see e.g. \citet{gilbert2004}),
so it is no surprise that agent-based modelling has been used to model emotional contagion. 
\citet{bosse2009} suggest a model where the emotional contagion strength
between agents $i$ and $j$ is represented by

\begin{align}
\gamma_{i,j} = \varepsilon_i \, \alpha_{i,j} \, \delta_j,
\label{eq:contagion}
\end{align}

where $\varepsilon_i$ is the strength by which the agent $i$ expresses its level of emotion.
This can be understood as the degree of introversion/extroversion of the agent.
Parameters $\alpha_{i,j}$ represent the connection strength between agents $i$ and $j$, which can be understood as
how close the social relationship between the agents is and how much they are interacting with each other. $\delta_j$
represents how easily the emotions of agent $j$ are affected by the emotions of others,
which can be interpreted as emotional sensitivity.

The overall emotional impact directed towards agent $j$ is then

\begin{align}
q_j^* = \sum_{i \neq j} \gamma_{i,j} q_i / \gamma_j,
\label{eq:impact}
\end{align}

where $q_i$ is the emotion level of agent $i$ and

\begin{align}
\gamma_j = \sum_{i \neq j} \gamma_{i,j}
\label{eq:contagion_strength}
\end{align}

is the overall emotional contagion strength. The interaction model in \citet{bosse2009}
is then

\begin{align}
q_j(t + \Delta t) = q_j(t) + \gamma_j (q_j^*(t) - q_j(t)) \Delta t.
\label{eq:basic_model}
\end{align}

In this model the emotional level of agent $j$ is updated towards the overall emotional impact directed
at the agent based on psychological results of mirroring and emotional contagion. The
magnitude of the update depends on the overall emotional contagion strength of the agent.

\citet{bosse2009spiral} extend the model by introducing a bias term $\beta_j$ representing whether the agent
is more susceptible to positive or negative emotional impacts. The interaction formula of
this model is then

\begin{align}
q_j(t + \Delta t) = q_j(t) + \gamma_j (\beta_j PI(t) + (1 - \beta_j)NI(t) - q_j(t)) \Delta t.
\label{eq:spiral_model}
\end{align}

This has again been extended in \citet{hoogendoorn2011}, where the parameter $\eta_j$ was introduced, representing
the tendency of agent $j$ to amplify or absorb the received emotion impacts, leading to an equation

\begin{align}
q_j(t + \Delta t) = q_j(t) + \gamma_j [\eta_j(\beta_j PI(t) + (1 - \beta_j)NI(t)) + (1 - \eta_j)q_j^*(t) - q_j(t)] \Delta t.
\label{eq:full_model}
\end{align}


\subsection{PoSITeams}
The original model behind the PoSITeams simulator was proposed by Juha Törmänen in his Bachelor's thesis (2011).
In Törmänen's model the positive and negative emotions are modeled separately and the interaction between the agents
happens through the positivity ratios

\begin{align}
\label{eq:juha_basic}
\begin{split}
P_i(t) &= (1 - a) P_i(t - 1) + \sum_{j \in A} c_{i,j}(t) P_j^{rel}(t - 1)\\
N_i(t) &= (1 - b) N_i(t - 1) + \sum_{j \in A} c_{i,j}(t) N_j^{rel}(t - 1),
\end{split}
\end{align}

where $c_{i,j} \in {0,1}$ indicates whether the agents $i$ and $j$ are connected and $P_j^{rel}(t - 1)$ and $N_j^{rel}(t - 1)$
denote the relative positivity ratios defined as

\begin{align}
\label{eq:juha_rel}
\begin{split}
P_j^{rel}(t) &= \frac{P_j(t)}{P_j(t) + N_j(t)}\\
N_j^{rel}(t) &= \frac{N_j(t)}{P_j(t) + N_j(t)}.
\end{split}
\end{align}

This model was further extended in Törmänen's work by introducing a negativity storage,
the size of which was defined as

\begin{align}
d_i(t) =
\left\{
	\begin{array}{ll}
		0 & \mbox{if } P_i(t)/N_i(t) < 3 \\
		1 & \mbox{if } 3 \leq P_i(t)/N_i(t) < 4 \\
		1 & \mbox{if } 4 \leq P_i(t)/N_i(t) < 5 \\
		1 & \mbox{if } 5 \leq P_i(t)/N_i(t)
	\end{array}
\right.
\label{eq:juha_container}
\end{align}

This leads to the following equations

\begin{align}
\label{eq:juha_full}
\begin{split}
P_i(t) &= (1 - a) P_i(t - 1) + \sum_{j \in A} c_{i,j}(t) P_j^{rel}(t - 1)\\
S_i(t) &= (1 - b) \max [0, S_i(t-1) - d_i(t - 1)] + \sum_{j \in A} c_{i,j}(t) N_i^{rel}(t-1)\\
N_i(t) &= (1 - b) N_i(t - 1) + \sum_{j \in A} c_{i,j}(t) N_j^{rel}(t - 1).
\end{split}
\end{align}

The negativity storage is motivated by the broaden-and-build theory \citep{fredrickson2001}, which states that
experiencing positive emotions increases resilience towards negative events through broadened
array of thoughts and actions that are available to the individual.

A further extension in the model uses the results of \citet{losada1999}, where it was observed
that the connectivity between the people in high-performance teams was higher than in other
teams. Therefore in Törmänen's work, the number of connections each agents has to its
neighbouring agents depends on its positivity ratio. Each agent has a priority list of its
nearest neighbours and the agent interacts with the $e_i(t)$ first agents in the order defined by the list, where

\begin{align}
e_i(t) =
\left\{
	\begin{array}{ll}
		0 & \mbox{if } P_i(t)/N_i(t) < 1 \\
		1 & \mbox{if } 1 \leq P_i(t)/N_i(t) < 3 \\
		3 & \mbox{if } 3 \leq P_i(t)/N_i(t) < 5 \\
		5 & \mbox{if } 5 \leq P_i(t)/N_i(t)
	\end{array}
\right.
\label{eq:juha_connectivity}
\end{align}

The underlying social network can therefore change as new connections can be formed or dropped as the simulation progresses.