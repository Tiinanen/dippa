\chapter{Modelling the contagion of emotions}
\label{chapter:model}

\section{Model development}

A new model is proposed to capture
the essential dynamics of emotional contagion in groups. Since this work focuses on positivity ratios and
its effects on organizations and social groups, elaborate models aiming to accurately reproduce the
variety of different emotions are not considered. The model focuses only on the level of positivity and negativity of emotions.
However, since there are some qualitative differences between
positive and negative emotions, such as broadening and narrowing of awareness and the negativity bias,
positive and negative emotions are represented as
separate variables as in the model proposed by Törmänen. It is also known that the
ratio of positive and negative emotions is the distinctive difference between flourishing and
non-flourishing individuals, so it is assumed that the agents interact with the other agents according
to their positivity ratios. Instead of directly using the positivity ratio $P/N$, the
proposed model uses $P^{rel}$ and $N^{rel}$ values as defined in \ref{eq:juha_rel} to limit the interaction
values within the range $[0, 1]$ and to avoid issues caused by the singularity of $P/N$ when $N \to 0$. 

Similarly to Gottman's model in \ref{eq:gottman}, the proposed model is of the form

\begin{align}
\label{eq:model_general}
\begin{split}
P_j(t+1) &= aP_j(t) + b + \sum_{i \neq j} I_{i,j}^P(t)\\
N_j(t+1) &= cN_j(t) + d + \sum_{i \neq j} I_{i,j}^N(t).
\end{split}
\end{align}

As in Gottman's model \ref{eq:gottman}, the positive and negative states can be separated into the influenced and uninfluenced
components. The influenced components are represented by the  \emph{influence functions} $I_{i,j}^P$ and
$I_{i,j}^N$, which characterize the interaction and emotional contagion between the agents $i$ and $j$, whereas the
remaining terms of the model \ref{eq:model_general} represent the uninfluenced part of the emotional state of
agent $j$. That is, the uninfluenced part represents the emotional state of the agent when there is no interaction between any other agents.
When the influence functions are set to zero and the agents are only affected by the uninfluenced component
of the model, then

\begin{align}
\label{eq:model_uninfluenced}
\begin{split}
P_j(t+1) &= aP_j(t) + b\\
N_j(t+1) &= cN_j(t) + d.
\end{split}
\end{align}

From this we get the following stable steady states for the model

\begin{align}
P_j &= \frac{b}{1 - a} \\
N_j &= \frac{d}{1 - c}.
\end{align}

Therefore it follows, that the stable steady state for the positivity ratio in the uninfluenced case is 

\begin{align}
\frac{P_j}{N_j} = \frac{b(1 -c)}{d(1 - a)}.
\label{eq:uninfluenced_steady_state}
\end{align}

The general positivity of the agent can be characterized by setting proper values for these
parameters, e.g. a flourishing person might have the ratio $b(1 - c)/(d(1 - a))$ of 3:1. Gottman calls
$a$ and $c$ emotional inertia parameters \citep{gottman2002}, which indicate how quickly the agent
returns to its steady state. Positive emotions tend to be more fleeting and short-lasting than negative
emotions \citep{baumeister2001}, so for most people it would be expected that $a > c$.  
It is also worth noting, that the uninfluenced case in \ref{eq:model_uninfluenced} have
solutions

\begin{align}
P_j(t) &= a^t P_j(0) + \frac{b(1 - a^t)}{1 - a}\\
N_j(t) &= c^t P_j(0) + \frac{d(1 - c^t)}{1 - c}.
\end{align}

Therefore $\frac{P_j}{N_j} \to \frac{b(1 - c)}{d(1 - a)}$ when $t \to \infty$, only if $|a| < 1$ and $|c| < 1$.

The positivity ratio of the model \ref{eq:model_general} increases, when

\begin{align}
\setlength{\jot}{1.5em}
\frac{P_j(t)}{N_j(t)} & < \frac{P_j(t + 1)}{N_j(t + 1)} = \frac{aP_j(t) + b + \sum_{i \neq j} I_{i,j}^P(t)}{cN_j(t) + d + \sum_{i \neq j} I_{i,j}^N(t)}\\
& \Rightarrow \frac{P_j(t)}{N_j(t)} \left[cN_j(t) + d + \sum_{i \neq j} I_{i,j}^N(t)\right] < aP_j(t) + b + \sum_{i \neq j} I_{i,j}^P(t)\\
& \Rightarrow cP_j(t) + \frac{P_j(t)}{N_j(t)}\left[d + \sum_{i \neq j} I_{i,j}^N(t)\right] < aP_j(t) + b + \sum_{i \neq j} I_{i,j}^P(t)\\
& \Rightarrow \frac{P_j(t)}{N_j(t)} < \frac{(a - c)P_j(t) + b + \sum_{i \neq j} I_{i,j}^P(t)}{d + \sum_{i \neq j} I_{i,j}^N(t)}.
\end{align}

Assuming $a = c$, the inequality is further simplified to

\begin{align}
\frac{P_j(t)}{N_j(t)} < \frac{b + \sum_{i \neq j} I_{i,j}^P(t)}{d + \sum_{i \neq j} I_{i,j}^N(t)}.
\label{eq:relation}
\end{align}

This shows that the change of P/N depends both on the agent's personal characteristics of $b$ and $d$ and the
external influences determined by the influence functions. Since it is assumed that $a = c$, the uninfluenced
steady state of the agent is simply $b/d$ as seen from the equation \ref{eq:uninfluenced_steady_state}.
Therefore if the values of $b$ and $d$ are large compared to the influence functions, P/N
converges towards the the uninfluenced steady state of the agent. In other words, by changing the absolute
values of $b$ and $d$ the behaviour of the model can be adjusted to either emphasize the impact of
the influence functions or the agent's general positivity determined by the uninfluenced steady state.
This is analogous to the "top-down" and "bottom-up" view of group emotions in \citet{barsade1998}.
The parameters $b$ and $d$ also keep P/N within a finite positive range, avoiding both zero and infinity.
This suggests that adjusting the $b$ and $d$ parameters also affects how volatile the behaviour of P/N is.

\subsection{Influence functions}
\label{chapter:influence_functions}

The proposed form for the influence functions is

\begin{align}
\label{eq:influence}
\begin{split}
I_{i,j}^P(t) &= \gamma_{i,j} \beta_j^P P_i^{rel}(t) \\ 
I_{i,j}^N(t) &= \gamma_{i,j} \beta_j^N N_i^{rel}(t).
\end{split}
\end{align}

That is, the emotional influence depends directly on the positivity ratio of the influencing agent.
The parameters $\beta_j^P$ and $\beta_j^N$ describe the negativity bias effect, i.e. a negative event has more
impact than a corresponding positive effect \citep{rozin2001}. Accordingly, the effects
of negative events are emphasized when $\beta_j^N > \beta_j^P$. Parameters $\beta_j^P$ and $\beta_j^N$
can be also interpreted as the different slope parameters in the bilinear influence function in
Gottman's model \ref{eq:gottman}. The model \ref{eq:full_model} also takes the negativity bias into account by weighting the
positive and negative emotional impacts with $\beta_j$ and $1 - \beta_j$.
Similarly, the number of parameters can be reduced in \ref{eq:influence} by setting $\beta_j = \beta_j^N = 1 - \beta_j^P$.
Parameter $\gamma_{i,j}$ describes the strength of emotional contagion. Similarly to \ref{eq:full_model}, it
is expressed as

\begin{align}
\gamma_{i,j} = \varepsilon_{i} \, \alpha_{i,j} \, \delta_{j}.
\label{eq:ec}
\end{align}

Here $\varepsilon_{i}$ describes how strongly agent $i$ expresses her emotional state to the other agents,
$\alpha_{i,j}$ represents the level of interaction between agents $i$ and $j$ and $\delta_{j}$ describes
how greatly the emotional level of agent $j$ is affected by the emotional influence of other agents.

A person might be
more extroverted among close friends than new acquaintances. Similarly the emotional influence of a spouse
is most likely stronger than that of one's neighbour. Therefore it could be possible to represent
$\varepsilon_i$ and $\delta_j$ in a more general form as $\varepsilon_{i,j}$ and $\delta_{j,i}$. That is,
the level of extroversion or emotional sensitivity would also depend on the other party of the interaction.
However, the simpler version \ref{eq:ec} is mathematically equally capable of representing differences in social
relationships with fewer parameters. Adjusting the parameter $\alpha_{i,j}$ is sufficient for representing
different emotional contagion strengths between pairs of agents.

The influence functions in \ref{eq:influence} are of the form

\begin{align}
I_{i,j}^P(t) &= f(P_i(t), N_i(t)) \\ 
I_{i,j}^N(t) &= g(P_i(t), N_i(t)).
\end{align}

An interesting generalization would be to allow the influence functions to depend on $P_j$ and $N_j$.
This way the behaviour of the agent $i$ depends not only on his current emotional state, but also the
previous emotional state of the agent $j$. This would allow the model to describe more complex behaviour, such as
systems of holding back \citep{saarinen2007}.

\subsection{Broaden-and-build extension}

The broadening effect of positivity is one of the main tenets of the broaden-and-build theory \citep{fredrickson2001}.
In Törmänen's model this was implemented by creating new connections between agents
as their positivity ratio grows, thus increasing the connectivity of the group as stated in \citet{losada2004}.
However, another approach is to increase both
$\delta_{j}$, which represents the emotional sensitivity of the agent, and $\varepsilon_i$ representing extroversion.
Increasing either of these parameters also increases the total emotional contagion strength
and thus increases the connectivity of the group as stated in \citet{losada2004}.
As the value of $\delta_j$ is increased, the effect of the group on the emotional state of the agent
also increases.
Thus the coupling between the agent and the whole group becomes stronger.
This is consistent with the results of \citet{waugh2006}, which states that increased positivity affects the feeling of "oneness" in the group.
Increasing the extroversion parameter $\varepsilon_i$ as the positivity ratio increases is also consistent with \citet{whelan2012}, which
states that positivity has a favourable effect on sociability.

The increase of $\delta_j$ and $\varepsilon_j$ is implemented with simple linear models

\begin{align}
\delta_{j}(t) =  P_j^{rel}(t - 1)(\delta_j^{max} - \delta_j^{min}) + \delta_j^{min}
\label{eq:openness_dynamic}
\end{align}
\begin{align}
\varepsilon_{j}(t) =  P_j^{rel}(t - 1)(\varepsilon_j^{max} - \varepsilon_j^{min}) + \varepsilon_j^{min}.
\label{eq:extroversion_dynamic}
\end{align}

It is assumed that $\delta_j^{min} < \delta_j^{max}$ and $\varepsilon_j^{min} < \varepsilon_j^{max}$ to ensure
that $\delta_j$ and $\varepsilon_j$ increase as the positivity ratio of the agent increases.
By increasing
the $\varepsilon_j$ and $\delta_j$ parameters, the increase in connectivity can be accounted
while keeping the underlying structure of the group fixed instead of creating or removing connections
as in Törmänen's model. This can be useful when simulating actual organizations with known social structures,
since the model corresponds more closely to the actual organizational structure.

The broaden-and-build theory states that experiencing positive emotions facilitates coping with
adversity \citep{fredrickson2003, garland2010}. In Törmänen's model \ref{eq:juha_basic}, 
this was implemented by introducing a negativity storage. Using the same approach as with $\delta_j$ and
$\varepsilon_j$ parameters, the proposed model varies the parameter $\beta_j$ so that it
depends linearly on the level of relative positivity $P^{rel}$. That is

\begin{align}
\beta_j(t) = P_j^{rel}(t - 1)(\beta_j^{min} - \beta_j^{max}) - \beta_j^{min} + 1.
\label{eq:beta_dynamic}
\end{align}

This makes the agent less susceptible to negativity when its positivity ratio increases.
Again it is assumed that $0 \leq \beta_j^{min} < \beta_j^{max} \leq 1$ so that the negative
bias decreases as the positivity ratio increases. This also ensures that $\beta_j^P + \beta_j^N = 1$
so that the sum of positive and negative influences stays constant, while their proportion 
varies depending on the level of negativity bias.

\section{Selecting the model parameters}
\label{chapter:params}
The presented emotional contagion model has several
free parameters, and some choices were made regarding which of these should
be adjustable by the user and which of them should be given a fixed value. Granting
the user too much freedom can be overwhelming and make it more difficult to
obtain insights from the application. Therefore the following choices
have been made.

\begin{itemize}
\item \emph{Emotional inertia}. This is denoted by parameters $a$ and $c$, which are
given a fixed value of 0.9. Although negative emotions tend to be longer lasting than
positive ones, these parameters are given the same value since the impact of negative
emotions is already emphasized by the negativity bias parameters.
A shared value also facilitates the reasoning of the model behaviour as shown in equation \ref{eq:relation}.

\item \emph{General positivity}. This corresponds to the positivity ratio in the
uninfluenced steady state of the agent given by equation $P/N = b(1 - c)/(d(1 - a))$.
That is, how positive the agent is when there is no interaction with any other agents in the system.
Since the emotional inertia parameters $a$ and $c$ are fixed, the general positivity is determined
by the parameters $b$ and $d$. The sum of $b$ and $d$ is
given a fixed value of $0.1$ and this is divided between the parameters $b$ and $d$ so that the
agent will have the general positivity level set by the user. Also the initial $P$ and $N$ levels
are determined so that their sum is $10$, which is divided between $P$ and $N$ so that the $P/N$
equals to the given general positivity value.

\item \emph{Extroversion}. This is the parameter $\varepsilon_j$ and can be
interpreted as the tendency to express one's emotional level to others. In the simulator this is
implemented as a linear function as defined in
equation \ref{eq:extroversion_dynamic} to take into account the increase in connectivity as
the positivity ratio increases. The user is allowed to adjust $\varepsilon_j^{min}$ between
$[0, 0.8]$ and $\varepsilon_j^{max}$ is fixed at $\varepsilon_j^{min} + 0.2$. That is,
the extroversion parameter is determined by 

\begin{align}
\varepsilon_{j}(t) =  0.2 \, P_j^{rel}(t - 1) + \varepsilon_j^{min}, \quad \varepsilon_j^{min} \in [0, 0.8].
\end{align}

\item \emph{Emotional sensitivity}. Corresponds to $\delta_j$, the tendency of agent's own
emotional level being affected by the emotions of others. This is implemented similarly
to extroversion using the linear equation \ref{eq:openness_dynamic} and the user
is allowed to change $\delta_j^{min}$ within range $[0, 0.8]$ and $\delta_j^{max}$ is
fixed at $\delta_j^{min} + 0.2$. This corresponds to equation

\begin{align}
\delta_{j}(t) =  0.2 \, P_j^{rel}(t - 1) + \delta_j^{min}, \quad \delta_j^{min} \in [0, 0.8].
\end{align}

\item \emph{Connection strength}. Determined by parameters $\alpha_{i,j}$, which represents
how strong the social relationship is between the agents $i$ and $j$. The user is allowed
to change this parameter between $[0,1]$.

\item \emph{Negativity bias}. To reduce free parameter this is defined as
$\beta_j = \beta_j^N = 1 - \beta_j^P$. As stated in the broaden-and-build theory,
experiencing positive emotions increases the capability to cope with negative emotions
and therefore the the negativity bias is changed according to 
the equation \ref{eq:beta_dynamic}. The user is allowed to change $\beta_j^{min}$
between $[0, 0.5]$ and $\beta_j^{max}$ is set to $\beta_j^{min} + 0.5$.
Negativity bias is therefore determined by

\begin{align}
\beta_j(t) =  -0.5 \, P_j^{rel}(t - 1) + 1 - \beta_j^{min}, \quad \beta_j^{min} \in [0, 0.5].
\end{align}

\end{itemize}
\section{How to best improve team behaviour}

Considering the emotional contagion model as an optimization problem, there are a number of interesting
problems to investigate, such as

\begin{itemize}
\item find the optimal behaviour that maximizes the individual or collective positivity ratio
\item find the optimal structure of the organization
\item what kind of a team member would be the best addition to the team in terms of maximizing the positivity ratio of the team
\end{itemize}

\subsection{Simulated annealing}
\label{chapter:optimization}

The optimization of the emotional contagion model is performed with simulated annealing for its simplicity.
Simulated annealing is an approximate global optimization technique that emulates the process of slowly cooling
a heated metal and thus minimizing its thermodynamic free energy \citep{kirkpatrick1983}.
A typical example of its application is the traveling salesman problem, a classic NP-hard problem \citep{vcerny1985}.

To use simulated annealing, each parameter value combination of the system is defined as a state. For simplicity,
all the parameters are considered to be discrete values within a predefined interval. A neighbouring state is
obtained from the current state by randomly changing a parameter value with a probability of

\begin{align}
P(|\Delta p| > 0) = \min\left(\frac{1}{2}, 1 - \frac{S - 2}{S}\right),
\end{align}

where $S$ is the number of parameters in the state. The expected number
of parameters to be changed is therefore 2, when $S \geq 4$. This is motivated by the assumption that 
good solutions are located near other good solutions. After a
number of iterations the current solution is presumably better than a randomly selected state and 
by doing a minor change in the current state, the obtained neighbouring state is also likely to be good.
Minor alterations to the current state hopefully improve the poor parts of the solution while keeping
the good parts mostly unchanged.

If a parameter value $p$ is changed, its new value $p'$ is selected uniformly from range

\begin{align}
p' \in [\max(l, p - 0.1N), \min(u, p + 0.1N)].
\end{align}

where $l$ and $u$ define the lower and upper bound of the parameter interval of length $N$.
Each state $s$ has an associated energy defined by the energy function $E(s)$. The algorithm
attempts to find the state with the lowest energy. The transition from state $s$ to its
neighbouring state $s'$ is accepted with probability

\begin{align}
P(e, e', T) =
\left\{
	\begin{array}{ll}
		1 & \mbox{if } e' \leq e \\
		\exp\left(-(e' - e)/T\right) & \mbox{if } e' > e,
	\end{array}
\right.
\end{align}

where $e$ and $e'$ are the energies of the states $s$ and $s'$ respectively.  $T$ is the temperature parameter,
which is initially set to $T_0 = 10^{10}$ and cooled according to $T_{n+1} = 0.999 \, T_{n}$ until $T < 0.01$.
This corresponds to 27618 iterations, which has been found out to be sufficiently quick and accurate in practice.

The energy function can be chosen rather freely.
It can be for example the negative P/N of a single agent or the negative mean P/N of all the agents.
The P/N value can be evaluated by performing simulations for each parameter combination until convergence.
However, reaching convergence can be slow in practice, so a predefined number of simulations is used to evaluate
P/N approximately. A value of 100 is used to provide satisfactory results in a reasonable time.
Other interesting energy functions could be negative of the minimum P/N of the group, so that the objective
is to maximize the lowest P/N in the group. 

The energy functions can incorporate costs associated with 
changing a parameter value. The motivation behind this is that there is always
some effort required in changing one's behaviour or social connections. Also some changes
are easier than others, for example increasing extroversion can be easier for someone than decreasing
her negativity bias.

The actual cost functions for $\Delta p$ are unknown, so it is assumed that each parameter $p$
has a cost $c^-$ for a negative change of one unit and a cost $c^+$ for a positive change of one unit.
This corresponds to a bilinear cost function $g_p$ as shown in figure \ref{fig:cost}.

\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}[scale=6]
    % Draw axes
    \draw [<->,thick] (0,1) node (yaxis) [above] {cost}
        |- (1,0) node (xaxis) [right] {$\Delta p$};
	% Draw labels
	\draw (0,0) node[anchor=north] {-1}
	(0.5,0) node[anchor=north] {$0$}
	(1,0) node[anchor=north] {1}
	(0,0.5) node[left] {$c^{-}$}
	(0,0.8) node[left] {$c^{+}$};
	
    % line
    \draw (0,0.5) coordinate (a_1) -- (0.5,0) coordinate (a_2);
	\draw (0.5,0) coordinate (a_2) -- (1,0.8) coordinate (a_3);
\end{tikzpicture}
\caption{An example of a bilinear cost function $g_p$ of changing the value of parameter $p$.
Each parameter $p$ is assigned a unique cost function to represent the effort of changing
the value of the parameter.}
\label{fig:cost}
\end{center}
\end{figure}

Including the costs in the optimization turns the problem into a multi-objective optimization problem. A common approach to multi-objective optimization
is to optimize a weighted sum of all the objectives \citep{marler2004}. The costs are therefore combined with a energy function $E(s)$ by

\begin{align}
E'(s) = (1 - w) E(s) + \frac{w}{M} \sum_{p \in s} g_p(\Delta p),
\label{eq:weighted_objective}
\end{align}

where $w \in [0, 1]$ is the trade-off between minimizing the original energy function and minimizing
the costs associated with changing any of the parameters. $M$ is the number of agents in the system,
hence the total cost is divided between all the agents (i.e. it is easier for two agents to do one
behavioural change each than for one agent to do two changes).

\subsection{Interpreting the optimization as a model of agent behaviour}

There are different approaches available for the optimization.
For example, the parameters of the model could be optimized with reinforcement learning, 
which has certain similarities with systems intelligence.
In systems intelligence a complete knowledge
of the state of the system or its internal dynamics are not assumed to be known. Similarly, in
reinforcement learning an agent has only a limited knowledge about the system itself. Reinforcement
learning also emphasizes that the agents are active components of the system, which have power to affect
the system and are also themselves affected by the system. This is a key concept in systems intelligence - 
one is within the system rather than observing it from the outside. Reinforcement learning
could therefore be used as a basis to implement systems intelligent agents and to model learning
organizations.

As with simulated annealing, reinforcement learning allows using
different emotional models to simulate the dynamics of emotions. Including additional constraints is
also straightforward. It could also be possible to create
models for entirely different systems and thus reinforcement learning could provide a framework for
general systems intelligence simulations. In emotional dynamics context the agents have a clear objective to
maximize the collective emotional state, rather than having the agents to deal with multiple, possibly
mutually contradicting objectives. However, reinforcement learning could possibly be used to implement
agent-specific objective functions. It is worth noting that in simulated annealing framework
all the agents share the same energy function
that they are minimizing. That is, all the agents are assumed to be collaborating with each other.
Realistically, people are likely to be more interested in their own well-being than a shared goal of the
whole organization. However, these two goals are highly correlated in our framework, since the
agent's well-being depends on the well-being of others. Therefore having a shared objective is not
necessarily a significant drawback.

However, multi-agent reinforcement learning has its challenges,
such as non-stationarity inflicted by the adaptation to the constantly changing behaviour of other learning agents
\citep{busoniu2008}. In a sense, the agents are attempting to learn a moving a target, which may cause problems.
Also the large state-space imposed by multiple agents can be problematic. Therefore simulated
annealing has been selected over reinforcement learning in this work.

