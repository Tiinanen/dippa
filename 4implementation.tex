\chapter{Implementation of the simulator}
\label{chapter:implementation}

\section{Technical details of the implementation}

The proposed model and the optimization methods described in chapter \ref{chapter:model}
were implemented as a web application using JavaScript, HTML5, CSS and SVG.
The application relies heavily on the D3.js
library\footnote{More information about the D3.js library can be found at
\url{http://d3js.org/}.},
which is widely used for interactive visualizations. Besides D3.js, also the
popular jQuery\footnote{For more information about the JQuery library, see \url{https://jquery.com/}} and jQuery UI\footnote{For more information about the JQuery UI library, see \url{https://jqueryui.com/}}
libraries were used in the application. General view of the simulator is shown in figure \ref{fig:simulator}. 

The program has been designed to be fairly modular and its main component are divided as follows:

\begin{itemize}
\item \verb|Agent|: A prototype that implements the functionality of a single agent. This functions as a container of the
agent parameters and calculates dynamically changing values such as extroversion, emotional sensitivity and negativity bias.
\item \verb|AgentGroup|: A container of the Agent objects, which also performs all the calculations of the simulation. This
prototype also implements functionality such as adding and removing agents or links. A history of positivity values is also
recorded. The optimization methods have been also included in this prototype, although they could be easily implemented as
a separate component.
\item \verb|AgentGraph|: Implements the graphical representation of the simulation. The visual representation
of the agents and their connections are implemented with SVG elements, which are coupled with the simulation results
using the D3.js library. It is worth noting that the actual computation of the simulations does not depend on
the graphical representation provided by AgentGraph, allowing the simulator to be coupled with other user interfaces.
AgentGraph only implements the essentials of the user interface, such as the graphical agent graph and the functionality
to select, add and delete agents and their connections. Additional features can be implemented using the
custom callback mechanism, which promotes loose coupling between the actual graphical representation of the simulation
and additional features such as optimization.
\item \verb|HistoryGraph|: A line chart plugin implemented with D3.js.
\item \verb|BarGraph|: An independent bar graph plugin implemented with D3.js.
\item \verb|positeams_base.js|: Implements the most basic features such as "Start", "Restart" and "Quick iterations" buttons.
also couples the AgentGraph object with HistoryGraph and BarGraph objects. All the functionality is implemented with the
callback mechanism of the AgentGraph, allowing AgentGraph to be independent of the most features related to the user interface.
\item \verb|positeams.js|: This implementes the optimization and parameter limit features on top of the positeams\_base.js
user interface. Again the callback mechanism allows extending the AgentGraph object easily with new features without actually
modifying its code.
\item \verb|utils.js|: Includes additional helper functions and prototypes, such as data structures for parameter limits and costs.

\end{itemize}

The whole program consists of about 3000 lines of JavaScript and a small amount of HTML and CSS.
Considerations for future extension have been taken into account. The event-like callback mechanism of AgentGroup
allows adding arbitrary code to certain key places, such as at the start of a new iteration or when adding a new agent.
New features can be therefore implemented without actually modifying the code of AgentGroup. Even completely new
applications, such as turning the simulator into a game, can be done with minimal modifications to AgentGraph,
AgentGroup and Agent prototypes.

\section{Features and the user interface}

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/simulator.png}
      \caption{The user interface of the simulator.}
  \label{fig:simulator}
 \end{center}
\end{figure}

\subsection{Visualizing and interacting with the model}

As described in chapter \ref{chapter:params}, the user is allowed to adjust

\begin{itemize}
\item General positivity
\item Extroversion
\item Emotional sensitivity
\item Connection strength
\item Negativity bias
\end{itemize}

To make the user interface simple, the parameter ranges are presented for
the user as sliders with values between $[0,1]$ and they are internally mapped to the ranges
described in chapter \ref{chapter:params}.
The only exception to this is the general positivity parameter, since it has a
direct interpretation as a positivity ratio.

Most of the simulator view is dedicated to visualizing the organization of the agents as seen in figure \ref{fig:simulator}.
The agents change their color from deep blue to bright yellow depending on their positivity ratio.
Similarly the facial expression of the agent varies dynamically from sad to happy depending on its
current positivity ratio. The positivity ratios are also drawn as a function of the iteration
number next to the agent graph, which turns into a bar graph when clicked as shown in figure
\ref{fig:bargraph}. Hovering the cursor over an agent highlights the corresponding
line or bar in the figure and shows also information about the agent underneath
the figure as shown in figure \ref{fig:bargraph}.

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=60mm]{images/bargraph.png}
      \caption{Alternative visualization for the positivity ratios. 
      The highlighted bar corresponds to the agent currently under the mouse cursor. 
      Below the graph is also shown additional information about the highlighted agent.}
  \label{fig:bargraph}
 \end{center}
\end{figure}

The connections between the agents are shown as links
in the directed graph and their opacity is directly proportional to the total emotional contagion
$\gamma_{i,j}$ between the agents. The length of the links also indicates the level of
interaction and the strength of the social relationship between the agents described
by the parameter $\alpha_{i,j}$, which enforces clustering of socially connected
groups. The graph is drawn using a force-directed graph layout algorithm provided
by the D3.js library, which makes the nodes repel each other to ensure a visually
pleasing graph. The user can also drag and move the graph by pressing the "CTRL" key
as she wishes.

New agents can be added by clicking the empty space around the graph and new
links can be formed between the agents by drawing a line between two agents with the mouse.
By holding the "SHIFT" key while creating a link, the user can also add the back edge simultaneously.
Agents and links can be selected by clicking them, which opens an interface to modify
the parameters of the selected object. The interface to adjust the agent parameters is shown
in figure \ref{fig:simulator} below the buttons and a similar view of adjusting link strength
is shown in figure \ref{fig:results1}. The selected object can be removed by pressing
the "delete" button.

To start the simulation, the user presses the "Start" button, which turns into a "Stop"
button while the simulation is running. The "Restart" button returns the simulation back to
its original state as it was before the first iteration, which allows the user to easily
explore the outcomes of different scenarios. The current iteration is shown
below the button row along with the current average positivity ratio of the agents. If the
user wishes to simulate a large number of iterations quickly, she can press the
"Quick simulations" button, which simulates 1000 iterations at once. This is useful to
quickly see the final steady states without waiting.

\subsection{Optimization}

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/results1.png}
      \caption{The results of the optimization can be seen from "Show optimization parameters". Below the results is also shown the view for adjusting the strength of the currently selected connection and the level of the emotional contagion strength $\gamma_{i,j}$.}
  \label{fig:results1}
 \end{center}
\end{figure}

The simulator provides three different optimization features. By pressing "Optimal action",
the simulator finds the optimal single action involving the currently selected agent.
This can be either a change in "General positivity", "Extroversion", "Emotional
sensitivity", "Negativity bias" or a change in the connection strength of any of the outgoing
links from the selected agent. Another optimization feature is "Optimize agent", which
optimizes all of the aforementioned parameters at once using simulated annealing. Similarly
"Optimize group" applies simulated annealing to all of the agents and their connections.
The results of the optimizations can be seen by pressing "Show optimization results" as shown
in figure \ref{fig:results1}.

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/limits1.png}
      \caption{The user interface of "Parameter limits" let's the user to adjust feasible parameter ranges for the agents.}
  \label{fig:limits1}
 \end{center}
\end{figure}

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/limits2.png}
      \caption{The ranges for the connection strength parameters can also be adjusted individually under "Parameter limits".}
  \label{fig:limits2}
 \end{center}
\end{figure}

The behaviour of the optimization features can be adjusted from "Parameter limits" and
"Optimization settings". The user can set fixed ranges for all the parameters
from "Parameter limits". For example the user might feel that it is infeasible for a certain agent
to behave in an overly extroverted manner. The interface for adjusting the agent parameters is
shown in figure \ref{fig:limits1}. The user can also set limits to connection strengths. For example
a work place might have separate teams and the interaction between them might be limited. Since the
number of possible connections increases quickly as the number of agents grows, the user can
easily adjust multiple connection strength limits at once by specifying several source and target agents
and setting a shared value to all of their combinations as shown in figure \ref{fig:limits2}.
The connection strength limits can also be adjusted one by one. Each of the agents can also
be given a range of total incoming or outgoing connections. For example a supervisor of a
team most likely has to have some level of interaction with the team, so having no
connections would be infeasible.

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/settings1.png}
      \caption{Behaviour of the optimization can be adjusted in "Optimization settings".}
  \label{fig:settings1}
 \end{center}
\end{figure}

More adjustments to the optimization methods can be done in "Optimization settings",
which provides the interface to select different objective functions, which agents
and parameters are being optimized and the costs associated with changing any of the parameters
as described in chapter \ref{chapter:optimization}. Figure \ref{fig:settings1} shows the general
settings, where the user can adjust the trade-off parameter labeled as "Effect of costs",
which corresponds to $w$ in equation \ref{eq:weighted_objective}. If this parameter is set to
zero, no costs are used and each change in parameters is equally feasible. The user
can also set the objective function, which has options "Own P/N", "Mean P/N" and
"Lowest P/N" for the single agent and "Mean P/N" as well as "Lowest P/N" for the group.
One can also select any subset of the agents for optimization, or only considering 
agent parameters or connection strength parameters in optimization. The user can
then adjust costs for each of the selected agents in "Agents to be optimized" as
shown in figure \ref{fig:settings2}. Similarly costs can be also adjusted for
each of the connection strengths as shown in figure \ref{fig:settings3}. The costs
correspond to $c^-$ and $c^+$ as shown in figure \ref{fig:cost}. That is, the left
side of the slider is the cost associated with changing the given parameter to a smaller
value and the right side to a larger value.

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=0.85\textwidth]{images/settings2.png}
      \caption{The interface for setting costs associated with changing each of the agent parameters. The first number corresponds to the cost associated with decreasing the parameter value by one unit and second to an increase of one unit.}
  \label{fig:settings2}
 \end{center}
\end{figure}

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=0.9\textwidth]{images/settings3.png}
      \caption{The costs can be adjusted for the connection strengths either in groups or individually.}
  \label{fig:settings3}
 \end{center}
\end{figure}




