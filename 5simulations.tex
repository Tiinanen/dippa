\chapter{Example simulations}
\label{chapter:simulations}

\section{A simple group}
\subsection{Emotion contagion spirals}

The first simulation example consists of three agents, one positive and two negative.
The agent parameters of the example are shown in table \ref{table:ex1}. All the connections
have a strength of 1, except there is no connection from Cecilia to Björn.
The figure \ref{fig:ex1b} shows the simulation at its steady state after around 200 iterations.
The average positivity ratio in this steady state is only 0.18,
which is much lower than the general positivity of any of the agents. Similar
behaviour can be observed in \citet{bosse2009spiral} using the model \ref{eq:spiral_model},
where the authors model emotion contagion spirals inspired by the broaden-and-build theory. The
behaviour in this simulation example can be considered an example of a negativity spiral
and it is also an example of a collective emotional state, which is not a sum of its
parts, consistent with the "top-down" view in \citet{barsade1998}.

\begin{table}[htb!]
\begin{tabular}{c|c|c|c|c}

Name & General positivity & Extroversion & Sensitivity & Negativity bias \\
\hline
Aapeli & 5 & 0.8 & 0.8 & 0.6 \\

Björn & 1 & 0.8 & 0.8 & 0.7 \\

Cecilia & 1 & 0.8 & 0.8 & 0.7 \\

\end{tabular}
\caption{The agent parameters of the first simulation example.}
\label{table:ex1}
\end{table}

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex1b.png}
      \caption{The first simulation example after reaching its steady state.}
  \label{fig:ex1b}
 \end{center}
\end{figure}

Next the "Optimize agent" feature is used to optimize Aapeli, restricting the optimization only
to adjustment of emotional sensitivity and extroversion. This leads to a change in emotional
sensitivity from 0.8 to 0 and extroversion from 0.8 to 1. Since Aapeli is the most positive
of the three agents with general positivity of 5, it is natural to adjust the emotional sensitivity
to a low value to self-generate positivity and cope with the external negativity.
Also increasing extroversion is beneficial to spread the
positivity in the system. These adjustments lead to a P/N of 5.53 as shown in figure \ref{fig:ex1c}.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex1c_100.png}
      \caption{The state of the system after optimizing the emotional sensitivity and extroversion
parameters of Aapeli.}
  \label{fig:ex1c}
 \end{center}
\end{figure}

Now the average positivity ratio is larger than the general positivity of any of the agents.
Again, this is similar to the model in \citet{bosse2009spiral} being an example of a positive
spiral. The main difference is that \citet{bosse2009spiral} have a stricter interpretation of
the emotional contagion as converging to the same shared emotional state.
In the example shown in figure \ref{fig:ex1c} the agents have different
steady states caused by individual differences, but they still represent a collective emotional
state and an example of a positive spiral. This is also consistent with the view in
\citet{barsade1998}, where the authors argue that studying group emotion should include
both views, the "top-down" view where the emotions of the individuals arise from the group
and the "bottom-up" view, where the group emotion is determined by a composition of the
emotions of the individuals.

\subsection{Path dependency}

An interesting behaviour happens when the optimization is applied again to Aapeli,
starting from the state shown in figure \ref{fig:ex1c}.
As a result of this, the emotional contagion is set from 0 to 1, which increases the
average P/N to 8.22. One might expect that setting the emotional sensitivity near the
original value would have a negative effect, returning the system near to its original
state. However, the difference is that the system
is not the same anymore and whereas in the beginning Aapeli was surrounded by negative
agents, now he is surrounded by positive ones. Being emotionally sensitive is
a positive quality in a positive environment since it let's one be influenced by the
surrounding positivity. Conversely, being emotionally stoic is beneficial in
a negative environment. This also demonstrates that it is not necessarily possible
to reach the global optimum of the system using a single optimization, since the optimal behaviour
in the global optimum might not be able to escape the initial negative steady state.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex1d_100.png}
      \caption{The state of the system after optimizing the emotional sensitivity and extroversion of Aapeli for the second time starting from the state shown in \ref{fig:ex1c}. This demonstrates that it can be impossible to reach the global optimum of the system with a single optimization. Also, changing the parameters to their original values does not necessarily return the system to its original state.}
  \label{fig:ex1d}
 \end{center}
\end{figure}

\subsection{Balancing the group and individual behaviour}

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex0.png}
      \caption{The first simulation example when the sum of the parameters $b$ and $d$ is fixed at $5$.}
  \label{fig:ex0}
 \end{center}
\end{figure}

The agents are affected both by their individual characteristics and their neighbouring
agents. This balance can be adjusted by the $b$ and $d$ parameters of the model as shown
by the inequality in \ref{eq:relation}.
In the simulator the sum of these parameters is fixed at 0.1. When this sum is
set to a larger value, the behaviour of the model is quite different. The figure
\ref{fig:ex0} shows an example when $b + d = 5$. Consistent with the results in equation
\ref{eq:relation}, the behaviour of the agents is largely determined by their general positivity
and their environment does not have much of an effect. 
The sum of these parameters is
therefore set to a small value in the simulator, since it shows more interesting behaviour by
incorporating both the "top-down" and "bottom-views" of collective emotions as stated \citet{barsade1998}.
This also allows the model to describe behaviour analogous with emotional contagion spirals.

\subsection{Bifurcation points}

A related phenomenon to emotion spirals is the
existence of bifurcation points, which can be demonstrated using the original example,
but setting Aapeli's negativity bias initially to 0.53 and 0.54 in consecutive runs. In these cases,
the system reaches an average P/N of 7.95 and 0.19 respectively as shown
in figures \ref{fig:ex2a} and \ref{fig:ex2b}, demonstrating the existence of bifurcation points.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex2a.png}
      \caption{A positive steady state is reached by setting Aapeli's negativity bias initially to 0.53.}
  \label{fig:ex2a}
 \end{center}
\end{figure}

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex2b.png}
      \caption{A negative steady state is reached by setting Aapeli's negativity bias initially to 0.54.}
  \label{fig:ex2b}
 \end{center}
\end{figure}

\subsection{Considerations in the optimization}

The optimization example in figure \ref{fig:ex1c} was performed by
applying 100 iterations for each parameter combination.
However, if this is increased to 1000, a better solution can be found,
although this makes the optimization uncomfortably slow.
The solution is to set emotional sensitivity to 0.11 and
extroversion to 1. This leads to average positivity ratio of 6.11, 
however, the onset to reach the steady state takes hundreds of
iterations as shown in figure \ref{fig:ex1c_1000}. 

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex1c.png}
      \caption{The solution when each parameter combination is evaluated with 1000 iterations.}
  \label{fig:ex1c_1000}
 \end{center}
\end{figure}

As demonstrated in the path dependency example in figure \ref{fig:ex1d},
it can be impossible to reach a global optimum by applying optimization only once.
This is because being emotionally sensitive is beneficial in the new positive steady state,
but detrimental in the initial negative steady state.
Therefore the solution obtained in figure \ref{fig:ex1c_1000} essentially balances between setting emotional
sensitivity to a low enough value to escape the negative steady state, but high enough to take advantage
of the benefits of high emotional sensitivity in the obtained positive steady state. Extrapolating
from this example, this behaviour is emphasized even more when the emotional sensitivity of Aapeli
is set to 0.13 and extroversion to 1 in the initial steady state shown in figure \ref{fig:ex1b}.
The positivity ratios stay almost unchanged for 2000 iterations, until the system escapes the negative
steady state and reaches an average positivity ratio of 6.20. This is still far from the global
optimum shown in figure \ref{fig:ex1d}. For practical purposes, solutions that provide quick changes are
therefore more preferable. This allows reaching the global optimum more quickly by applying multiple
rounds of optimizations. Waiting for an extended period of time just to escape the initial
negative steady state and to reach a state that is far from the global optimum is rather pointless.
The number of iterations to evaluate each parameter combination in the optimization
is thus fixed to 100.

\section{A small organization}
\label{chapter:organization}
\subsection{Optimization with zero costs}
A more complicated example is shown in figure \ref{fig:ex3a}, which consists of
two small teams with a shared supervisor. The agent parameters of the
example are shown in table \ref{table:ex3}. All the connections shown
in the figure have a strength of 1. Team A consists of Aapeli, Antti
and Aurora, whereas team B is formed by Barbara and Björn. Cecilia is the supervisor
of the two teams. To enforce the team structure in the simulation, the
parameter limits are set so that connections within each team must be
in range $[0.5, 1]$ and between the teams within $[0, 0.1]$. Cecilia must
have connection strengths in the range $[0.2, 1]$ with all the members in the organization.
The general positivity of all the agents is also constrained between $[0, 5]$
and the negativity bias must be within $[0.5, 1]$. The whole group
is then optimized without parameter costs, which leads to a
steady state shown in figure \ref{fig:ex3b} with an average positivity ratio
of 32.71. Detailed optimization results can be found in the Appendix \ref{app:zero}.

\begin{table}[htb!]
\begin{tabular}{c|c|c|c|c}
Name & General positivity & Extroversion & Sensitivity & Negativity bias \\
\hline
Aapeli & 2 & 0.2 & 0.2 & 0.6 \\

Antti & 5 & 0.8 & 0.8 & 0.6 \\

Aurora & 3 & 0.2 & 0.8 & 0.6 \\

Barbara & 2 & 0.3 & 0.3 & 0.6 \\

Björn & 2 & 0.3 & 0.3 & 0.6 \\

Cecilia & 1 & 0.9 & 0.4 & 0.8 \\
\end{tabular}
\caption{The agent parameters of the organization simulation example.}
\label{table:ex3}
\end{table}

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex3a.png}
      \caption{Steady state of the small team before optimization with an average P/N of 0.33.}
  \label{fig:ex3a}
 \end{center}
\end{figure}

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex3b_100.png}
      \caption{The result of optimization with no parameter costs.}
  \label{fig:ex3b}
 \end{center}
\end{figure}

Although the exact solution varies between subsequent runs, since
simulated annealing is an approximate global optimization method, the general
trend is minimizing negativity bias, maximizing general positivity
and strong emotional connection strengths. However, the "trivial solution"
of setting extroversion, emotional sensitivity and general positivity to
maximum and negativity bias to minimum fails to escape the negative
steady state as shown in figure \ref{fig:ex3b_trivial}. 

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex3_trivial.png}
      \caption{The "trivial solution" fails to escape the negative steady state and
leads to an average positivity ratio of 0.12.}
  \label{fig:ex3b_trivial}
 \end{center}
\end{figure}

\subsection{Optimizing connection strengths}
Another type of solution can be obtained
by only optimizing the connection strengths, attempting to find an optimal organizational structure.
Again no parameter costs are used
and the limits are kept the same as for the previous example. This
leads to a solution shown in \ref{fig:ex3_connections}. Detailed optimization results
can be found in Appendix \ref{app:connections}.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex3_connections.png}
      \caption{The results of optimizing the connection strengths of the group.}
  \label{fig:ex3_connections}
 \end{center}
\end{figure}

The main characteristic of the obtained solution is that the connection
strengths from Cecilia to other agents are minimized. According to the general positivity
parameter, Cecilia is the most negative person in the group. Thus
the solution is to restrict the flow of negativity originating from her by
decreasing emotional contagion strengths.

\subsection{Optimization with costs}

As described in chapter \ref{chapter:optimization}, costs can be 
assigned to changing each of the model parameters, taking
into account the effort associated with changing one's behaviour
or social relationships. These costs are assigned for the following simulation
example as shown in figure \ref{fig:ex3b_params}. In addition, all the connection
strengths are given small costs $c^+$ and $c^-$ of 1. Since the solution
of the optimization without costs invariably maximizes positivity ratios and minimizes 
negativity biases, large costs are assigned to these parameters
to moderate their effect. The solution
obtained from this experiment with the trade-off ("Effect of costs") set to 50\%
 is shown in figure \ref{fig:ex3b_weights}. Detailed results can be found
 in Appendix \ref{app:costmean}.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex3_params2.png}
      \caption{The costs associated with changing the value of the parameter.
The first number corresponds to the cost of decreasing the parameter value by one unit ($c^-$)
and the second corresponds to the increase of one unit ($c^+$).}
  \label{fig:ex3b_params}
 \end{center}
\end{figure}

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex3_tradeoff50_run2.png}
      \caption{The solution of the optimization with costs.}
  \label{fig:ex3b_weights}
 \end{center}
\end{figure}

Two interesting aspects of the obtained solution is the weak connections
strengths from Cecilia and strong connection strengths from
Antti, who is the most positive of the agents based on general positivity.
Antti is given a more central role to benefit from his positivity, while
Cecilia's role is diminished.

The same example can be solved using "Lowest P/N" as the objective function.
The solution is quite similar in both cases, but it can be seen from the
P/N graph in figure \ref{fig:ex3b_weights_lowest} that the positivity
ratios do not deviate as dramatically as when maximizing the mean P/N.
Detailed results are found in Appendix \ref{app:costlow}.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/sim_ex3_t50_lowest2.png}
      \caption{The solution of the optimization with costs using the lowest P/N as the objective to be maximized.}
  \label{fig:ex3b_weights_lowest}
 \end{center}
\end{figure}

\subsection{Adding a new team member}

The last example examines the possibility of adding a new member, Boris, to
the team B. The question is, what kind of person Boris should be and
what should be his role so that the average positivity ratio of the group is maximized?
Boris is first connected to all of the members in team B and to Cecilia as shown
in figure \ref{fig:boris}. The initial connection strengths are irrelevant, since
they are immediately optimized. The connection strengths between Boris and team A are
limited within range $[0, 0.1]$ to enforce the structure of two separate teams.
Other connections are left unconstrained.

All the parameters are given zero costs to give different solutions
equal weights. In this example, Boris is to be considered a pseudo member rather than
an actual team member with personal characteristics. Thus changing the
parameter values does not correspond to changing the behaviour of an actual team member
and there is no cost associated with changing one's behaviour.

Boris is optimized by selecting him as the only member for group optimization, so that
only his agent parameters and incoming and outgoing connections are being optimized. 
The solution of the optimization is shown in figure \ref{fig:boris2}, with an average P/N of 7.86.
Detailed optimization are found in Appendix \ref{app:boris1}.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/boris.png}
      \caption{The initial state when adding a new member to the team B.}
  \label{fig:boris}
 \end{center}
\end{figure}

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/boris2.png}
      \caption{The solution of adding an optimal team member, Boris, to the group. The mean P/N eventually converges to 7.86.}
  \label{fig:boris2}
 \end{center}
\end{figure}

The solution is to give Boris a role where he strongly communicates towards his team members,
while keeping a certain distance so that he is not himself affected by the negativity of the group.
This rather one-sided communication channel might not be especially realistic in practice, but
alternative solutions can be found by adjusting the costs and parameters limits and exploring
different outcomes. One possibility is to set the lower bound of the total incoming connections to 1.
This leads to a solution shown in \ref{fig:boris3}. Detailed results can be found in Appendix \ref{app:boris2}. The solution takes about 1000 iterations to escape
the negative steady state, but eventually a steady state with a mean P/N of 8.82 is reached. This
is even higher than in the previous example, but qualitatively the solutions are quite similar.
The incoming connections are divided between Björn and Barbara since they are more positive than Cecilia.
However, the emotional sensitivity of Boris is set to zero and therefore hardly any emotional contagion
occurs, allowing Boris to spread positivity in the network without being affected by the negativity of
the organization.

\begin{figure}[htb!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/boris3.png}
      \caption{The solution after optimizing Boris with the lower bound of the total incoming connection strengths set to 1.}
  \label{fig:boris3}
 \end{center}
\end{figure}
