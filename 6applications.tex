\chapter{Applications}
\label{chapter:applications}

The model
described in this work has not been yet validated with any real world data,
so it is unknown how well it describes emotional contagion as it occurs in organizations.
Therefore any
predictions and quantitative values that the model gives remain theoretical.
However, even if the model cannot be directly used to predict emotional developments in organizations, there are
still several plausible applications for the model.

One application for the model is to demonstrate plausible systemic effects that can occur in organizations.
For example feedback loops in emotional contagion can be made more evident by playing with the simulator.
There has been growing interest to develop technology to promote emotional well-being \citep{calvo2014}.
Instead of promoting mental faculties such as mindfulness, empathy or compassion, PoSITeams could be
used as a tool to promote systems intelligence, systems thinking and emotional intelligence.

Applications that focus on increasing mental well-being are often designed to resemble games (see e.g. \citet{mccallum2012}),
a concept known as gamification, which aims at making the application highly engaging and fun. Perhaps
a potential use case for PoSITeams would be to make it more game-like. For example the user could be
given different social groups and corresponding tasks, such as maximizing the overall positivity of
the given group. The approach of posing the user problems that she must solve could be more beneficial
in terms of promoting systems intelligence. Also the user would be dealing with "imaginary" social groups,
which might make it easier to consider actions that do not come naturally to the user. In actual 
social groups there can be reservations, e.g. "my workplace does not allow me to be more introverted", which
might make the user more reluctant to consider alternative behavioural modes.

In \citet{bosse2009spiral} an ambient agent model is proposed for an emotional contagion model,
where the model would be given emotional level inputs from a group, for example by analyzing facial images,
and it would give action proposals to the team leader in case group emotion level drops below
a certain level. That is, the emotional contagion models could be utilized to help regulate emotions in organizations.
In a similar manner the emotional contagion model could be combined with sentiment analysis, which would
make information channels such as e-mail and social media attainable for emotional contagion modelling.

Emotional contagion has been observed in social media (see e.g. \citet{kramer2014})
 and by modelling and simulating the phenomenon it could be
possible to design social media platforms to better support mental well-being. For example
the visibility of content that promotes contagion of positive emotions could be adjusted.

\section{Case study: Emotional contagion in the Enron email dataset}
\label{chapter:enron}

The Enron Corpus \citep{klimt2004} is a large dataset of emails from the Enron Corporation.
It contains over 500 000 messages from about 150 employees\footnote{August 21, 2009 version of the dataset 
from \url{http://www.cs.cmu.edu/~enron/} was used in the analysis. This is a cleaner
version of the original dataset described in \citet{klimt2004}.}
although many of these are duplicates.
The corpus was made public by the Federal Energy Regulatory Commission
during its investigation of the collapse of the Enron Corporation and it
is still one of the only publicly available datasets of this kind.

The Enron Corpus has been used for various research purposes, such as
spam filtering \citep{fumera2006}, automatic email categorization \citep{bekkerman2004}
and social network analysis \citep{diesner2005, tang2008}).
In this work a simplified version of the proposed emotional contagion
model was applied to the Enron Corpus with the purpose of determining
whether the model could be applied to email data. The motivation for
this is that communication by email within an organization has similarities with
typical face-to-face communication. 
Emotional contagion has also been previously observed in social media
such as Facebook \citep{kramer2014} and blogosphere \citep{zafarani2010},
so actual face-to-face interaction is not necessary for emotional contagion to occur
and it seems plausible that emotions propagate through emails as well.

Although a more suitable
dataset for model validation purposes would be to have actual measurements of 
emotional responses of people during social interactions using, for
example, facial electromyography \citep{dimberg1990}, email data is more readily available and
it could potentially have major applications besides model validation 
when combined with emotional contagion models. For example, it could be possible
to monitor the performance of the organization by obtaining real-time emotional data from
the email traffic, which could be used in decision support. Similarly personal email data
could be used to monitor well-being of individuals. Such data could also be used to
evaluate the outcomes of organizational interventions and to design systems that
promote flourishing.

However, utilizing email data has quite a few challenges to overcome.
One of problems is that the emotional state of the sender is not explicitly
visible in the message. In this work sentiment analysis was used as an
attempt to overcome this problem. The idea is that each sentence is
automatically classified as positive/neutral/negative and the emotion
level of the sender is estimated by aggregating the sentiments of the
individual sentences. Of course there is a fair amount of variance and noise
in the aggregated sentiments and subsequent messages might vary widely
in their sentiment values. Therefore 10-day averages have been used to
reduce randomness in the sentiment time series.

Other technical problems are how to deal with forwarded and replied
messages. For the replied messages, only the latest reply of the message was
attempted to be extracted. The format of the messages may vary, so
it is possible that this automatic extraction does not cover all cases.
The forwarded messages were not parsed in any way, since usually the
content in the forwarded part of the message is of interest for the receiver.

In the Enron Corpus, there were also problems such as multiple email aliases, which were not
explicitly stated anywhere. These were manually figured out by collecting
all email addresses in each person's email folders, especially the different
addresses that are in "to", "cc" and "bcc" fields,
and these email addresses were sorted by number of occurrences.
From the top 20 addresses, those were
selected that clearly resembled the person's name. It is also worth noting,
that it is not guaranteed that the email folder of each person actually
contains only messages from that person. For example the sent folder
might contain messages from other people. One possible explanation could
be that a person has a secretary handling their emails. Therefore
it is not sufficient to assume that all the messages in the sent folders
are actually sent by the person to whom this folder supposedly belongs to.

\subsection{Sentiment analysis}

There are several algorithms that have been used for sentiment analysis.
Machine learning methods have been widely used in sentiment analysis \citep{pang2002},
and the state of the art methods utilize advanced techniques,
such as deep learning \citep{socher2013}. However, such methods are examples
of supervised learning, which require labeled data to train the model \citep{pang2008}.
The Enron Corpus has no labeled sentences, and manually labeling the sentences would be
tedious. The models learnt by supervised learning are highly domain
dependent and they rarely perform well in other domains \citep{pang2008}. There are also
domain independent approaches (see e.g. \citet{turney2002, taboada2011}),
which are often lexicon-based
rather than employing statistical machine learning. The idea behind lexicon-based
methods is to build a lexicon of unambiguous sentiment words, such as
"horrible", or "excellent", and to use the occurrence of such words to classify
sentences. These lexicons can be built manually or using for example \emph{seed words}
such as "excellent" and "poor" and use different rules to infer other sentiment words
from the data, with the idea that words with similar sentiment tend to occur more frequently
together \citep{turney2002}.
The domain dependent approaches are generally
more accurate than the domain independent methods, simply because
some words may have different semantic interpretation in different contexts.
For example the word "cold" has a positive sentiment in "a cold beer"
a negative sentiment in "a cold pizza". For a more in-depth coverage of
sentiment analysis, see for example \citet{pang2008, liu2012}.

In this work the sentiment analysis method was kept simple, since building
a complex sentiment analysis model is beyond the
scope of this preliminary study of the feasibility of using email data
with emotional contagion models. The absolute accuracy of the sentiment analysis
is also not of the highest priority. Thus a simple
domain-independent lexicon-based approach was employed using the
Pattern library provided by CLiPS 
(Computational Linguistics and Psycholinguistics Research Center)\footnote{More information
about the Pattern library is available at \url{http://www.clips.ua.ac.be/pages/pattern}.}.
The method has a reported accuracy of 75\% for movie reviews\footnote{The accuracy
is reported at \url{http://www.clips.ua.ac.be/pages/pattern-en\#sentiment}. Accessed 5th May 2015.}.
The accuracy for the email data was only superficially inspected for random sentences,
which seemed to be of acceptable level for this study. Lexicon-based sentiment analysis
has been previously applied to Enron Corpus in \citet{mohammad2011}. The Pattern library
reports the sentiment values within range $[-1, 1]$ and these values were used directly
rather than classifying the sentences to positive or negative classes and counting the number of
such sentences. The motivation of directly using the reported sentiment values is that
many of the sentences are rather neutral and ignoring this neutral class forces the
sentences to be classified either positive or negative, which can be problematic (see e.g.
\citet{koppel2006}). Also introducing an arbitrary limit for positivity (such as the
sentence is classified positive if its sentiment value is over 0.1) seems unwise since
a poorly chosen limit either discards useful data or incorrectly classifies neutral sentences
to either class. Such limits should be therefore decided based on actual classification results.
Thus the sentiment scores are used directly, which avoids all of the aforementioned problems,
since neutral sentences have only a minor effect, but useful data is not discarded altogether 
either.
The sentiment scores were aggregated so that the absolute values of the positive and negative
scores were separately summed together. These sums, $p$ and $n$, were then used to estimate
the $P$ and $N$ values of the sender by

\begin{align}
\hat{P} = \frac{p}{p + n}\\
\hat{N} = \frac{n}{p + n}.
\end{align}

Thus the length of the emails does not have any effect and the values $\hat{P}$ and $\hat{N}$
are within range $[0, 1]$, increasing the robustness of the following regression analysis.

\subsection{Regression analysis}

Once the emails were parsed and turned into sentiment values,
a matrix was built between the users based on number the received and
sent messages. Only the emails sent between the employees in the Enron Corpus
were considered in the matrix. From the matrix it is easy to see which
employees communicate a lot with each other and it is possible to select only
a small number of the closest coworkers for the statistical analysis.

The aggregated sentiment scores $\hat{P}$ and $\hat{N}$ were directly equated with
the $P$ and $N$ values after computing a 10-day average. That is, the person's level of
positivity and negativity
are considered known from the email observations. This crude assumption is not
entirely justified, but it greatly simplifies the statistical
analysis, which can be performed with a simple linear regression model.
It would be more justified to treat the $P/N$ ratio as a latent variable
that affects the aggregated sentiment observations. However, the estimated
$\hat{P}$ and $\hat{N}$ are presumed to be correlated with the actual values,
which is why such a simple approach is a reasonable initial try to study the
feasibility of applying emotional contagion models to email data.

Using this approach, we can build a linear regression model

\begin{align}
\label{eq:regression}
\begin{split}
P_j(t) = a_0 + a_1 P_j(t-1) + a_2 P_{C(1)}^{rel}(t-1) + \dots + a_n P_{C(n-1)}^{rel}(t-1)\\
N_j(t) = b_0 + b_1 N_j(t-1) + b_2 N_{C(1)}^{rel}(t-1) + \dots + b_n N_{C(n-1)}^{rel}(t-1)
\end{split}
\end{align}

where $C(i)$ is the $i$th closest coworker of $j$ . The regression coefficients $a_2,\dots,a_n$
encompass the parameters $\gamma_{i,j} = \varepsilon_{i} \, \alpha_{i,j} \, \delta_{j}$ and
 $\beta_i^P$ for the positivity and $\beta_i^N$ for the negativity.
This simple regression does not take into account the broaden-and-build effect to keep
the analysis simple.

\subsection{Results}

The regression model \ref{eq:regression} was fitted to a number of different persons
one at the time using the $k$ "closest" coworkers, when closeness in this context is defined as
the number of sent emails to the said person. The parameters were estimated using
ordinary least squares and all the missing values were replaced with zeros, which corresponds
to no communication between the persons.
The criteria for selecting a person was a large number of sent messages so that their
$\hat{P}$ and $\hat{N}$ values could be reasonably estimated and also the number of
received emails from the $k$ closest coworkers should be large so that there is enough
data for the statistical analysis. Different values of $k$ were tried, but typically
only those coworkers were selected that had sent over 100
emails to the person in question, which corresponds to $k$ between 2 and 5.
However, the results of the regression analysis were similar in all cases,
so only the results of a single example case are shown.

The following results were obtained by fitting the regression model to the emails of
taylor-m and five of his closest coworkers. The coworkers and the number of emails they
have sent to taylor-m is shown in table \ref{table:mail_stats}.
The number of sent emails by taylor-m is 1054, which is on the large end in the dataset.

\bgroup
\def\arraystretch{1.5}
\begin{table}[h!]
\begin{center}
\begin{tabular}{|c|c|}
\hline
Person & Sent messages \\
\hline
stclair-c & 308 \\
\hline
jones-t & 231 \\
\hline
shackleton-s & 197 \\
\hline
panus-s & 163 \\
\hline
kitchen-l & 137 \\
\hline
\end{tabular}
\end{center}
\caption{Enron employees that sent the most messages to taylor-m.}
\label{table:mail_stats}
\end{table}
\bgroup
\def\arraystretch{1}

The fitted model is shown in figure \ref{fig:model_fit}.
The figure shows that the data is quite noisy and the fit is not
particularly good ($R^2 = 0.1932$ for positivity and $0.2238$ for negativity).
The regression parameters are shown is \ref{table:parameters}. It can be seen
from the table that only the bias terms $a_0$ and $b_0$ and
the coefficients $a_0$ and $b_0$ corresponding to the lagged time series of
taylor-m's positivity and negativity values are clearly statistically significant,
whereas all the other coefficients are close to zero, with only $b_6$ being
statistically significant. Changing the number of predictors does not
particularly change the situation and even just by leaving only the bias terms
and the lagged time series of taylor-m, the model has a similar fitness. Therefore
taylor-m's estimated emotional level cannot be explained by the emotional levels of his coworkers.

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/model_fit.pdf}
      \caption{The linear regression model fitted to the email data of taylor-m.}
  \label{fig:model_fit}
 \end{center}
\end{figure}

\bgroup
\def\arraystretch{1.5}
\begin{table}[h!]
\begin{center}
\begin{tabular}{c|cc|cc}
\multicolumn{1}{c}{} & \multicolumn{2}{c}{Positive} & \multicolumn{2}{c}{Negative} \\
\hline
Variable & $a_i$ & 95\% CI & $b_i$ & 95\% CI \\
\hline
bias & 0.42 & 0.30 to 0.55 & 0.21 & 0.15 to 0.27 \\
\hline
taylor-m (lagged) & 0.39 & 0.21 to 0.57 & 0.33 & 0.15 to 0.51 \\
\hline
stclair-c & 0.012 & -0.041 to 0.065 & -0.014 &  -0.10 to 0.073 \\
\hline
jones-t & 0.012 & -0.037 to 0.060 & -0.0704 & -0.14 to 0.0040 \\
\hline
shackleton-s & 0.0056 & -0.050 to 0.061 & 0.022 & -0.047 to 0.091 \\
\hline
panus-s & 0.045 & -0.010 to 0.010 & -0.17 & -0.32 to -0.022 \\
\hline
kitchen-l & -0.010 & -0.055 to 0.034 & 0.0093 & -0.068 to 0.086 \\
\hline
\end{tabular}
\end{center}
\caption{The parameters of the fitted regression model.}
\label{table:parameters}
\end{table}
\bgroup
\def\arraystretch{1}

The model was also evaluated by performing residual analysis. The residuals plot in figure \ref{fig:residual}
shows that the residuals are quite symmetrically distributed and there are no clear patterns,
which seems to indicate that the residuals are consistent with random error. Autocorrelation
of the residuals was also investigated to test the regression assumption that the errors are
independent. Time series data usually breaks this assumption, however,
the regression model includes a lagged version of $P$ and $N$, which seems to reasonably capture the
autoregressive behaviour of the time series as seen in figure \ref{fig:autocorr}. Therefore modelling
the error terms as autoregressive processes, a common approach in linear regression of time series,
was considered unnecessary. Also the normality of the residuals was evaluated in figure
\ref{fig:normplot}, which seems to indicate that errors are reasonably well normally distributed.

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=\textwidth]{images/residual_plot.pdf}
      \caption{Residual plot of the fitted model.}
  \label{fig:residual}
 \end{center}
\end{figure}

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=0.95\textwidth]{images/autocorr.pdf}
      \caption{Autocorrelations of the residuals.}
  \label{fig:autocorr}
 \end{center}
\end{figure}

\begin{figure}[h!]
  \begin{center}
      \includegraphics[width=0.95\textwidth]{images/normplot.pdf}
      \caption{Normality probability plot of the residuals.}
  \label{fig:normplot}
 \end{center}
\end{figure}

From these results it therefore cannot be concluded that emotional contagion occurs through
email communication and neither can these results be used for validation of the proposed
emotional contagion model.
There are several factors that make studying emotional contagion through email communication
a difficult problem, and perhaps email data is not suitable at all for such purposes.
First of all email communication does not form a closed system and it is likely that most
of the emotional contagion that occurs between people happens through other channels.
Even within an organization such as Enron, it is likely that most of the communication happens
face-to-face and email does not play a crucial role. Also it can be questioned how truthfully people
express their actual emotions in their emails, especially in organizational environment.
For example, one would probably hold one's tongue when communicating with a supervisor.
Additional problems are introduced with sentiment analysis and estimating emotional levels
from the emails. The sentiment analysis method employed in this preliminary study was rather
simple and in no way optimized for the Enron Corpus. Better sentiment analysis methods might
provide better results. Also the statistical analysis was kept simple, and further improvements
could be obtained by more sophisticated methods, such as employing latent variable models.
Since the residuals of the models are well-behaving, it could indicate that the data
itself is simply too noisy, which means that improving the quality of the data extraction
process might make employing emotional contagion models to email data a viable endeavour,
but further studies are required to make any conclusions.