\chapter{Conclusions}
\label{chapter:discussion}

As the main purpose of this work was to provide a tool that can support systems intelligent
behaviour in organizations, it is worthwhile to reflect to what degree this goal has been reached.
Since systems intelligence consists of eight dimensions \citep{beingbetterbetter},
one can consider how well each of these dimensions is accounted for in the simulator. 

\begin{itemize}
\item \emph{Systems perception:} the simulator presents the organization as a graph, which draws the
attention to the relationships between the members of the organization. The graph
presentation also focuses on the holistic view of the organization by providing a view
of the whole organization at once.

\item \emph{Attunement:} The simulator draws attention to how our own behaviour can affect
the whole
organization. Therefore it encourages the user to be more aware of her behaviour. Also
the simulations themselves can be rather engaging and perceiving
oneself visually as a part of the whole can increase awareness of the systemic
nature of social groups and possibilities that may ensue. Together all of this have
a potential to motivate the user to be more attuned to her environment.

\item \emph{Reflection:} The user is encouraged to reflect on her own 
behaviour and relationships with others
as she provides parameter values to the model.

\item \emph{Positive engagement:} Since the application simulates 
emotional contagion and focuses on
the positivity ratios and its effects on organizational performance, the user is encouraged
to interact positively with other people.

\item \emph{Spirited discovery:} The simulator offers possible scenarios, 
promoting "what-if" thinking
and providing food for thought. The focus is also on embracing change and finding concrete
actions to change the system for the better. This forms a solid foundation for spirited discovery.

\item \emph{Effective responsiveness:} The simulator can identify 
leverage points in the organization
either by letting the user to explore various behavioural and structural changes or
by using the optimization functionality provided by the simulator. Effective measures can
be then taken to target the leverage points.

\item \emph{Wise action:} By using the simulator, the user hopefully 
obtains a better understanding of
the organization as a whole and how it can be affected by our own behaviour. The
systems perspective also attempts to demonstrate typical features of systems, such as
feedbacks, time delays, non-linearities, which ideally transforms into deeper understanding
of systems and therefore wiser actions.

\item \emph{Positive attitude:} Again, the simulator focuses on the effects of positivity,
which encourages an overall positive attitude.

\end{itemize}

The simulator therefore has elements supporting each of the eight dimension. Of course
this does not necessarily mean that using the simulator leads to more systems intelligent
behaviour, but it remains a viable hypothesis.

Another interesting question is whether using the simulator actually leads to better
actions at the organizational level. Peter Senge identifies five key features of 
learning organizations in his book The Fifth Discipline \citep{senge1990}:
personal mastery, mental models, shared vision, team learning and systems thinking.
The simulator promotes each of these disciplines to a certain degree: 

\begin{itemize}
\item \emph{Personal Mastery:} The simulator promotes personal growth by demonstrating
how changing each of the personal characteristics can improve both personal well-being
and organizational performance.

\item \emph{Mental models:} The systems perspective presents a new way to
visualize and think about the organization, which can challenge old ways
of thinking and acting.

\item \emph{Shared vision:} Improving positivity ratios provides a shared goal for the
organization. Since improving the positivity ratio within the organization has
also individual benefits, it is an easy goal to commit to.

\item \emph{Team learning:} Using the simulator in collaboration can promote
discussion and challenge old assumptions about the organization.

\item \emph{Systems thinking:} The systems philosophy is deeply ingrained within
the simulator and using the simulator highly promotes thinking about the organization
as a system. The simulator can even considered to be a tool to promote systems thinking itself.
\end{itemize}

It has been shown in chapter \ref{chapter:simulations} that the simulator is capable of
reproducing real-world phenomena such as emotional contagion spirals. The model is also
able to incorporate and balance the effects of both the individual characteristics and
the collective emergent group behaviour. The optimization procedures can also provide
interesting solution proposals in simple organizations. The simulator therefore
has a good basis for supporting systems intelligent behaviour in organizations. However,
it still lacks user experiences in the real-world and it would be interesting to test
whether a tool like this can actually generate change and 
whether the users find it useful. For future research, it would be interesting
to conduct a study and test the actual effects of using the simulator in organizations.
It would be also interesting to test whether using the simulator actually increases
systems intelligence.

Another direction for further research could be to improve and validate the emotional contagion
model presented in this work. At the moment, the model has not been validated with any
real data, and therefore the degree to which it actually represent reality is unknown. 
It would also be interesting to incorporate external data in the model. An preliminary
attempt to apply the emotional contagion model to email data was presented in chapter \ref{chapter:enron}.
Although the results were inconclusive, being able to extract positivity
data from sources such as email or social media and using it in emotional contagion
models has great potential.

The simulator software itself could also be developed further, for example to provide new features
such as being able to save and load social groups and results of different scenarios.
There could also be organizational accounts for using the simulator more collaboratively.
Everyone in the organization could estimate their own behaviour and their relationships
with the other members of the organization. Then each member could explore
different behavioural and structural changes in this network.
Another interesting approach would be to gamify the simulator, so that it could present
different social groups as problems to be solved. This way of using the simulator might
be able useful to practice systems intelligence as a mental faculty.

Designing better organizations could be also one potential direction for research.
Perhaps organizations that are robust to adversities share some structural characteristics
that can be explored with the simulator. Most organizations are not designed to support
individual well-being. However, since positivity and effective organizations are connected, designing the
organizations to embrace the effects of positivity seems like a worthwhile endeavour.